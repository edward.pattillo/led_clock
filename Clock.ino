
// -------------------------- customise -----------------------------

// RGB values from:
// http://paletton.com/#uid=55f0N0kwSv0kcCtpEy7zuoqEGj7
int hourRGB[] = {206, 0, 97};
int minuteRGB[] = {247, 100, 0};
int secondRGB[] = {80, 14, 168};

int brightness = 200; // default brightness is an integer from 0 - 255
int dimPercent = 6; // dim LEDs to what % of original value? integer from 0 - 100
bool fill_seconds = true;  // set to false if you don't want the seconds to fill up and reset

// ----------------- includes ---------------------------------------
#include <RTClib.h>
#include <Wire.h>
#include <FastLED.h>

// ---------------- FastLED Declarations ----------------------------

// pin LED data line to D5
#define LED_PIN 5
#define NUM_LEDS 60
#define LED_TYPE WS2812B
#define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];

#define UPDATES_PER_SECOND 1

// --------------- things --------------------------------------------

// initialise clock module
RTC_DS3231 rtc;

// for serial output
char t[32];

int lastHour = 13;
int lastMinute = 13;
int lastSecond = 13;
double dimF = dimPercent / 100.0;
int dimmedness = round(brightness * dimF);

// --------------- setup --------------------------------------------

void setup()
{
  Serial.begin(9600);
  Wire.begin();

  rtc.begin();
  rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  // the line below allows you to manually set time if you want:   rtc.adjust(DateTime(YYYY, MM, DD, HH, MM, SS));
//      rtc.adjust(DateTime(2019, 01, 20, 10, 59, 50));

  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS);
}

// --------------- main loop --------------------------------------------

void loop()
{
  DateTime now = rtc.now();

  // get the time and assign each value to an integer variable
  int thisHour = now.hour();
  int thisMinute = now.minute();
  int thisSecond = now.second();

  // serial output. uncomment lines below to have a look at time via serial baud 9600
  //  sprintf(t, "%02d:%02d:%02d %02d/%02d/%02d",  thisHour, thisMinute, thisSecond, now.day(), now.month(), now.year());
  //  Serial.print(F("Date/Time: "));
  //  Serial.println(t);

  // convert from 24 hour time to 12 hour time
  if (thisHour > 12) {
    thisHour -= 12;
  }

  // so that hour LED shows up in the right space
  int ledHour = thisHour * 5;

  // LEDs are ordered from 0-59. We don't have a "60" value.
  if (ledHour == 60) {
    ledHour = 0;
  }

  if (thisMinute == 60) {
    thisMinute = 0;
  }

  if (thisSecond == 60) {
    thisSecond = 0;
  }


  // -----------------------   seconds ------------------------------

  // checks for changes in seconds and updates accordingly
  // (works the same as the hour section above)
  if (thisSecond != lastSecond) {

    // We don't need to see every second in the serial output, but if you want to, uncomment the lines below.
    // Serial.print(F("thisSecond: "));
    // Serial.println(thisSecond);

    if (fill_seconds != true) {
      leds[lastSecond].setRGB(0, 0, 0);
      leds[lastMinute].setRGB(0, 0, 0);
      leds[lastHour].setRGB(0, 0, 0);

    }  else {

      if (thisSecond == 0) {

        switchOffAllLEDs(ledHour);
        leds[ledHour].setRGB(hourRGB[0], hourRGB[1], hourRGB[2]);
        leds[ledHour].nscale8_video(brightness);

        leds[thisMinute].setRGB(minuteRGB[0], minuteRGB[1], minuteRGB[2]);
        leds[thisMinute].nscale8_video(brightness);
      }
    }

    // do this every second

    if (thisSecond == thisMinute || thisSecond == ledHour) {

      lightBlender(thisSecond, secondRGB, minuteRGB);

    } else {
      leds[thisMinute].setRGB(minuteRGB[0], minuteRGB[1], minuteRGB[2]);
      leds[thisMinute].nscale8_video(brightness);
      lastMinute = thisMinute;
    }

    leds[ledHour].setRGB(hourRGB[0], hourRGB[1], hourRGB[2]);
    leds[ledHour].nscale8_video(brightness);
    lastHour = ledHour;

    leds[thisSecond].setRGB(secondRGB[0], secondRGB[1], secondRGB[2]);
    leds[thisSecond].nscale8_video(brightness);

    if (lastSecond != thisMinute && lastSecond != ledHour) {
      leds[lastSecond].nscale8_video(dimmedness);
    }

    lastSecond = thisSecond;

  }



  // ---------------  things ----------------------------------------------

  // update LED display
  FastLED.show();

  // one tenth of a second delay between cycles (makes our clock accurate to 1/10th a second)
  delay(100);

  // uncomment the next line to do a "one-off" reset of all LEDs to off state (read below)
  //    switchOffAllLEDs();
}




// ---------------  functions ----------------------------------------------


// blend two RBG colours of LEDs and show them
void lightBlender(int ledNum, int firstRGB[], int secondRGB[]) {

  int newRGB[] = {blendSingleColour(firstRGB[0], secondRGB[0]), blendSingleColour(firstRGB[1], secondRGB[1]), blendSingleColour(firstRGB[2], secondRGB[2])};

  leds[ledNum].setRGB(newRGB[0], newRGB[1], newRGB[2]);
  leds[ledNum].nscale8_video(brightness);
}





// does calculations to determine average R,G and B values
int blendSingleColour(int firstColour, int secondColour) {

  int avgColour = (firstColour + secondColour) / 2;

  if (avgColour > 255) {
    avgColour -= 255;
  }
  
  //  Serial.print(F("avgColour: "));
  //  Serial.println(avgColour);

  return avgColour;
}





// sometimes LED strips have persistent memory
// uncomment ledNum assignment to a hardcoded value then
// run this function and it will wipe them all to black (off)
void switchOffAllLEDs(int ledHour) {

  int ledNum = NUM_LEDS;
  //  int ledNum = 1000;

  for (int i = 0; i <= ledNum; i++) {

    if (i != ledHour) {
      leds[i] = CRGB (0, 0, 0);
      leds[i].nscale8_video(255);
      delay(10);
      FastLED.show();
    }

  }
  Serial.println("Switched off all LEDs.");
}
